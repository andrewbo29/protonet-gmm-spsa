# coding=utf-8
import numpy as np


def sps_estimate(support_set, query_set, q=1, M=8, max_num=10):
    process_set = query_set
    # process_set = support_set

    betas = []
    for i in range(M - 1):
        betas.append(np.where(np.random.rand(len(process_set)) > 0.5, 1, -1))

    if len(support_set.shape) == 1:
        proto = support_set
    else:
        proto = np.mean(support_set, axis=0)

    mul = 3
    it_num = 0
    while it_num < max_num:
        f = np.random.multivariate_normal(proto, np.eye(proto.shape[0]) * mul)

        h_sq_list = [np.sum([f - process_set[k] for k in range(len(process_set))]) ** 2]
        for j in range(M - 1):
            h_func = np.sum([betas[j][k] * (f - process_set[k]) for k in range(len(process_set))])
            h_sq_list.append(h_func ** 2)

        rank = np.arange(len(h_sq_list))[(np.argsort(h_sq_list) == 0)][0]

        if rank <= M - q:
            # if it_num > 0:
            #     print(it_num)
            return f

        it_num += 1

    return proto

