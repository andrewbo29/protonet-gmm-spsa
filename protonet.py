import torch.nn as nn
import torch.nn.functional as F


def conv_block(in_channels, out_channels):
    '''
    returns a block conv-bn-relu-pool
    '''
    return nn.Sequential(
        nn.Conv2d(in_channels, out_channels, 3, padding=1),
        nn.BatchNorm2d(out_channels),
        nn.ReLU(),
        nn.MaxPool2d(2)
    )


class ProtoNetOmniglot(nn.Module):
    '''
    Model as described in the reference paper,
    source: https://github.com/jakesnell/prototypical-networks/blob/f0c48808e496989d01db59f86d4449d7aee9ab0c/protonets/models/few_shot.py#L62-L84
    '''
    def __init__(self, x_dim=1, hid_dim=64, z_dim=64):
        super(ProtoNetOmniglot, self).__init__()
        self.encoder = nn.Sequential(
            conv_block(x_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, z_dim),
        )

    def forward(self, x):
        x = self.encoder(x)
        return x.view(x.size(0), -1)


class ProtoNetFCOmniglot(nn.Module):
    def __init__(self, x_dim=1, hid_dim=64, z_dim=64):
        super(ProtoNetFCOmniglot, self).__init__()
        self.encoder = nn.Sequential(
            conv_block(x_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, z_dim),
        )

        # latent_size = 32
        latent_size = 8

        self.fc = nn.Linear(z_dim, latent_size, False)

        # self.dropout = nn.Dropout(p=0.5)

    def forward(self, x):
        x = self.encoder(x)
        x = x.view(x.size(0), -1)
        # x = self.dropout(x)
        x = self.fc(x)
        return x


class ProtoNetMultiTaskOmniglot(nn.Module):
    def __init__(self, x_dim=1, hid_dim=64, z_dim=64, task_num=1):
        super(ProtoNetMultiTaskOmniglot, self).__init__()
        self.encoder = nn.Sequential(
            conv_block(x_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, z_dim),
        )

        latent_size = 64

        self.fc_task = []
        for _ in range(task_num):
            self.fc_task.append(nn.Linear(z_dim, latent_size, False))

    def forward(self, x):
        x = self.encoder(x)
        x = x.view(x.size(0), -1)

        out = []
        for fc in self.fc_task:
            out.append(fc(x))

        return out


class ProtoNetImagenet(nn.Module):
    '''
    Model as described in the reference paper,
    source: https://github.com/jakesnell/prototypical-networks/blob/f0c48808e496989d01db59f86d4449d7aee9ab0c/protonets/models/few_shot.py#L62-L84
    '''
    def __init__(self, x_dim=3, hid_dim=84, z_dim=64):
        super(ProtoNetImagenet, self).__init__()
        self.encoder = nn.Sequential(
            conv_block(x_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, hid_dim),
            conv_block(hid_dim, z_dim),
        )

    def forward(self, x):
        x = self.encoder(x)
        return x.view(x.size(0), -1)


class ProtoNetSoftNNOmniglot(nn.Module):

    def __init__(self, x_dim=1, hid_dim=64, z_dim=64):
        super(ProtoNetSoftNNOmniglot, self).__init__()
        self.conv1 = conv_block(x_dim, hid_dim)
        self.conv2 = conv_block(hid_dim, hid_dim)
        self.conv3 = conv_block(hid_dim, hid_dim)
        self.conv4 = conv_block(hid_dim, z_dim)

    def forward(self, x):
        out1 = self.conv1(x)
        out2 = self.conv2(out1)
        out3 = self.conv3(out2)
        out4 = self.conv4(out3)

        return out1.view(out1.size(0), -1), out2.view(out2.size(0), -1), out3.view(out3.size(0), -1), \
               out4.view(out4.size(0), -1)




