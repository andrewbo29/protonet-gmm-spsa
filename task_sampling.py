# coding=utf-8
import numpy as np
from sklearn.metrics import pairwise_distances


def mean_dist(opt, tasks):
    mean_tasks = np.zeros((opt.task_number, tasks[0].size(1)))
    for i, task_protos in enumerate(tasks):
        mean_tasks[i] = task_protos.mean(0).detach().numpy()

    tasks_ind = np.argsort(pairwise_distances(mean_tasks).mean(1))[::-1]
    # tasks_ind = np.argsort(pairwise_distances(mean_tasks).mean(1))

    return tasks_ind


def max_dist(opt, tasks):
    protos = []
    task_inds = []

    for i, task_protos in enumerate(tasks):
        for proto in task_protos:
            protos.append(proto.detach().numpy())
            task_inds.append(i)

    protos = np.array(protos)
    task_inds = np.array(task_inds)

    protos_sort_ind = np.argsort(pairwise_distances(protos).mean(1))[::-1]

    tasks_ind = []
    for ind in task_inds[protos_sort_ind]:
        if ind not in tasks_ind:
            tasks_ind.append(ind)

    return np.array(tasks_ind)


def sample_tasks_indexes(opt, tasks):
    # tasks_ind = mean_dist(opt, tasks)
    tasks_ind = max_dist(opt, tasks)

    return tasks_ind[:opt.top_task_number]
