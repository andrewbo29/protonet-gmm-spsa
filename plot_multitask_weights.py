import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns
import pandas as pd
from scipy.spatial.distance import euclidean, cosine
from scipy import linalg
import os
from scipy.stats import cauchy, halfcauchy
import argparse
import sys


COLORS = ['navy', 'darkgreen', 'darkred']
# sns.set_style("whitegrid")


def plot_weights(data):
    sns.set_style("darkgrid")

    plt.figure()

    # plt.plot(data[0], '--', linewidth=5)
    # plt.plot(data[1], '-.', linewidth=5)
    # plt.plot(data[2], ':', linewidth=5)

    for i in range(len(data)):
        plt.plot(data[i], '--', linewidth=5)

    plt.legend(['Weight %d' % (i + 1) for i in range(len(data))])

    # plt.xlabel('Iterations')
    plt.xlabel('Epochs')

    plt.ylabel('Weight value')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-data')

    args = parser.parse_args()

    data = np.load(args.data)
    plot_weights(data)

    plt.show()


if __name__ == '__main__':
    sys.exit(main())


