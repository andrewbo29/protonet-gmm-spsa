# coding=utf-8
from __future__ import print_function
import torch.utils.data as data
import numpy as np
import os
from PIL import Image
import pickle
from torchvision import transforms


class MiniImagenetDataset(data.Dataset):

    def __init__(self, mode='train', root='../dataset', transform=transforms.Compose([transforms.ToTensor()]),
                 target_transform=None):
        super(MiniImagenetDataset, self).__init__()
        self.root = root
        self.transform = transform
        self.target_transform = target_transform

        print('\n== Dataset mode: {}'.format(mode))

        image_class_dict = {}

        data_file = os.path.join(self.root, 'mini-imagenet-cache-train.pkl')
        with open(data_file, 'rb') as handle:
            image_class_dict['train'] = pickle.load(handle)

        data_file = os.path.join(self.root, 'mini-imagenet-cache-val.pkl')
        with open(data_file, 'rb') as handle:
            image_class_dict['val'] = pickle.load(handle)

        data_file = os.path.join(self.root, 'mini-imagenet-cache-test.pkl')
        with open(data_file, 'rb') as handle:
            image_class_dict['test'] = pickle.load(handle)

        self.x = []

        for d in image_class_dict[mode]['image_data']:
            self.x.append(Image.fromarray(d.astype('uint8'), 'RGB'))

        label_ind = {}
        ix = 0
        for split_mode in image_class_dict:
            for label in image_class_dict[split_mode]['class_dict']:
                label_ind[label] = ix
                ix += 1

        self.y = np.zeros(len(self.x), dtype=int)

        for label in image_class_dict[mode]['class_dict']:
            ind = image_class_dict[mode]['class_dict'][label]
            self.y[ind] = label_ind[label]

        print("== Dataset: Found %d items " % len(self.x))
        print("== Dataset: Found %d classes" % len(image_class_dict[mode]['class_dict']))

    def __getitem__(self, idx):
        x = self.transform(self.x[idx])
        return x, self.y[idx]

    def __len__(self):
        return len(self.x)


if __name__ == '__main__':
    mode = 'val'

    dataset = MiniImagenetDataset(mode=mode, root='/home/a.boiarov/data/meta-learning/mini-imagenet')
    # print(dataset.x.shape)