# coding=utf-8
from prototypical_batch_sampler import PrototypicalBatchSampler
from omniglot_dataset import OmniglotDataset
from imagenet_datasets import MiniImagenetDataset
from protonet import *
import torch
from prototypical_loss import prototypical_loss as loss_fn, prototypical_loss_scale as loss_fn_scale
import numpy as np
from parser import get_parser
from tqdm import tqdm
import os
import torch.nn as nn
import time

from tracking_optimize import Tracking
import multiclass_weights_optimize
import task_sampling


def init_seed(opt):
    '''
    Disable cudnn to maximize reproducibility
    '''
    torch.cuda.cudnn_enabled = False
    np.random.seed(opt.manual_seed)
    torch.manual_seed(opt.manual_seed)
    torch.cuda.manual_seed(opt.manual_seed)


def init_dataset(opt):
    '''
    Initialize the datasets, samplers and dataloaders
    '''

    if opt.dataset == 'omniglot':
        train_dataset = OmniglotDataset(mode='train', root=opt.dataset_root, exp_type=opt.omniglot_type)

        val_dataset = OmniglotDataset(mode='val', root=opt.dataset_root, exp_type=opt.omniglot_type)

        # trainval_dataset = OmniglotDataset(mode='trainval', root=opt.dataset_root, exp_type=opt.omniglot_type)

        test_dataset = OmniglotDataset(mode='test', root=opt.dataset_root, exp_type=opt.omniglot_type)

    elif opt.dataset == 'mini-imagenet':
        train_dataset = MiniImagenetDataset(mode='train', root=opt.dataset_root)
        val_dataset = MiniImagenetDataset(mode='val', root=opt.dataset_root)
        test_dataset = MiniImagenetDataset(mode='test', root=opt.dataset_root)

    is_within_alphabet = False
    if opt.omniglot_type.endswith('within'):
        is_within_alphabet = True

    if opt.tracking:
        tr_sampler = PrototypicalBatchSampler(labels=train_dataset.y,
                                              classes_per_it=opt.classes_per_it_tr,
                                              num_samples=opt.num_support_tr + opt.num_query_tr,
                                              iterations=2 * opt.iterations, alphabets=train_dataset.alphabets,
                                              is_within_alphabet=is_within_alphabet)
    else:
        tr_sampler = PrototypicalBatchSampler(labels=train_dataset.y,
                                              classes_per_it=opt.classes_per_it_tr,
                                              num_samples=opt.num_support_tr + opt.num_query_tr,
                                              iterations=opt.iterations, alphabets=train_dataset.alphabets,
                                              is_within_alphabet=is_within_alphabet)

    is_multiloss = False

    if opt.multiloss or opt.multiloss_pretrain or opt.multiloss_grad_all or opt.multiloss_grad_one \
            or opt.multiloss_sampling or opt.multiloss_pretrain_grad_one or opt.multiloss_track \
            or opt.multiloss_track_pretrain or opt.multiloss_sampling_pretrain:

        is_multiloss = True

    if is_multiloss:
        tr_sampler_help = PrototypicalBatchSampler(labels=train_dataset.y,
                                              classes_per_it=opt.classes_per_it_tr,
                                              num_samples=opt.num_support_tr + opt.num_query_tr,
                                              iterations=opt.task_number * opt.iterations, alphabets=train_dataset.alphabets,
                                              is_within_alphabet=is_within_alphabet)

    val_sampler = PrototypicalBatchSampler(labels=val_dataset.y,
                                           classes_per_it=opt.classes_per_it_val,
                                           num_samples=opt.num_support_val + opt.num_query_val,
                                           iterations=opt.iterations, alphabets=val_dataset.alphabets,
                                           is_within_alphabet=is_within_alphabet)

    # trainval_sampler = PrototypicalBatchSampler(labels=trainval_dataset.y,
    #                                             classes_per_it=opt.classes_per_it_tr,
    #                                             num_samples=opt.num_support_tr + opt.num_query_tr,
    #                                             iterations=opt.iterations)

    test_sampler = PrototypicalBatchSampler(labels=test_dataset.y,
                                            classes_per_it=opt.classes_per_it_val,
                                            num_samples=opt.num_support_val + opt.num_query_val,
                                            iterations=opt.iterations, alphabets=test_dataset.alphabets,
                                            is_within_alphabet=is_within_alphabet)

    tr_dataloader = torch.utils.data.DataLoader(train_dataset,
                                                batch_sampler=tr_sampler)

    val_dataloader = torch.utils.data.DataLoader(val_dataset,
                                                 batch_sampler=val_sampler)

    # trainval_dataloader = torch.utils.data.DataLoader(trainval_dataset,
    #                                                   batch_sampler=trainval_sampler)

    test_dataloader = torch.utils.data.DataLoader(test_dataset,
                                                  batch_sampler=test_sampler)

    if is_multiloss:
        tr_dataloader_help = torch.utils.data.DataLoader(train_dataset, batch_sampler=tr_sampler_help)

        return tr_dataloader, val_dataloader, test_dataloader, tr_dataloader_help

    # return tr_dataloader, val_dataloader, trainval_dataloader, test_dataloader
    return tr_dataloader, val_dataloader, test_dataloader


def init_protonet(opt):
    '''
    Initialize the ProtoNet
    '''
    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'
    if opt.dataset == 'omniglot':
        if opt.soft_nn:
            model = ProtoNetSoftNNOmniglot().to(device)
        elif opt.tracking:
            model = ProtoNetFCOmniglot().to(device)
        else:
            model = ProtoNetOmniglot().to(device)
            # model = ProtoNetFCOmniglot().to(device)
    elif opt.dataset == 'mini-imagenet' or opt.dataset == 'tiered-imagenet':
        model = ProtoNetImagenet().to(device)

    return model


def init_optim(opt, model):
    '''
    Initialize optimizer
    '''

    optimizer = torch.optim.Adam(params=model.parameters(), lr=opt.learning_rate, weight_decay=opt.weight_decay)

    optimizer_next = None
    if opt.tracking:
        params = [{'params': model.fc.parameters()}]
        optimizer_next = torch.optim.Adam(params=params, lr=opt.learning_rate,
                                                weight_decay=opt.weight_decay)
    elif opt.multiloss_pretrain or opt.multiloss_pretrain_grad_one or opt.multiloss_track_pretrain \
            or opt.multiloss_sampling_pretrain:
        optimizer_next = torch.optim.Adam(params=model.parameters(), lr=opt.learning_rate * 0.01, weight_decay=opt.weight_decay)

    return optimizer, optimizer_next


def init_lr_scheduler(opt, optim):
    '''
    Initialize the learning rate scheduler
    '''
    return torch.optim.lr_scheduler.StepLR(optimizer=optim,
                                           gamma=opt.lr_scheduler_gamma,
                                           step_size=opt.lr_scheduler_step)


def save_list_to_file(path, thelist):
    with open(path, 'w') as f:
        for item in thelist:
            f.write("%s\n" % item)


def train(opt, tr_dataloader, model, optim, lr_scheduler, val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        model.train()
        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            loss.backward()
            optim.step()
            train_loss.append(loss.item())
            train_acc.append(acc.item())
        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_multi_loss(opt, tr_dataloader, tr_dataloader_help, model, optim, lr_scheduler, val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    iteration_num = 0

    weights = np.array([1 / opt.task_number for _ in range(opt.task_number)])
    # weights = np.array([np.sqrt(opt.task_number) for _ in range(opt.task_number)])
    # weights = np.random.uniform(1e-5, 1e-2, size=opt.task_number)

    # epsilon = np.random.normal(loc=0, scale=0.1, size=opt.task_number)
    # weights = np.array([1 / opt.task_number + epsilon[i] for i in range(opt.task_number)])

    weights_traces = []
    weights_traces_epochs = []
    for weight in weights:
        weights_traces.append([weight])
        weights_traces_epochs.append([weight])

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        tr_iter_help = iter(tr_dataloader_help)
        model.train()

        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            losses_all = [loss]
            acc_all = [acc]
            for it in range(1, opt.task_number):
                batch_help = next(tr_iter_help)

                x_help, y_help = batch_help
                x_help, y_help = x_help.to(device), y_help.to(device)
                model_output_help = model(x_help)
                loss_help, acc_help, _ = loss_fn(model_output_help, target=y_help, n_support=opt.num_support_tr, args=opt, is_test=False)

                losses_all.append(loss_help)
                acc_all.append(acc_help)

            loss_all = 0
            for j, loss_val in enumerate(losses_all):
                loss_all += 1 / (float(weights[j]) ** 2) * loss_val + np.log(weights[j] ** 2)
                # loss_all += 1 / (float(weights[j]) ** 4) * loss_val + np.log(weights[j] ** 2)

            loss_all.backward()
            optim.step()

            train_loss.append(loss_all.item() / len(losses_all))
            train_acc.append(np.mean([acc.item() for acc in acc_all]))

            if iteration_num > 0:
                weights = multiclass_weights_optimize.optimize(weights, losses_all, iteration_num)

                for i, weight in enumerate(weights):
                    weights_traces[i].append(weight)
            # print(weights)

            iteration_num += 1

        # iteration_num += 1

        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

        for i, weight in enumerate(weights):
            weights_traces_epochs[i].append(weight)

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    np.save(os.path.join(opt.experiment_root, 'weights_traces.npy'), np.array(weights_traces))
    np.save(os.path.join(opt.experiment_root, 'weights_traces_epochs.npy'), np.array(weights_traces_epochs))

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_multi_loss_pretrain(opt, tr_dataloader, tr_dataloader_help, model, optims, lr_scheduler, val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    print('=== Pretraining all net ===')

    optim = optims[0]

    for epoch in range(opt.epochs_all):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        model.train()
        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            loss.backward()
            optim.step()
            train_loss.append(loss.item())
            train_acc.append(acc.item())
        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    print('\n=== Training multiloss ===')

    optim = optims[1]

    iteration_num = 0

    weights = np.array([1 / opt.task_number for _ in range(opt.task_number)])

    # weights = np.array([np.sqrt(opt.task_number) for _ in range(opt.task_number)])
    # weights = np.random.uniform(1e-5, 1e-2, size=opt.task_number)

    weights_traces = []
    weights_traces_epochs = []
    for weight in weights:
        weights_traces.append([weight])
        weights_traces_epochs.append([weight])

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        tr_iter_help = iter(tr_dataloader_help)
        model.train()

        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            losses_all = [loss]
            acc_all = [acc]
            for it in range(1, opt.task_number):
                batch_help = next(tr_iter_help)

                x_help, y_help = batch_help
                x_help, y_help = x_help.to(device), y_help.to(device)
                model_output_help = model(x_help)
                loss_help, acc_help, _ = loss_fn(model_output_help, target=y_help, n_support=opt.num_support_tr, args=opt, is_test=False)

                losses_all.append(loss_help)
                acc_all.append(acc_help)

            loss_all = 0
            for j, loss_val in enumerate(losses_all):
                loss_all += 1 / (float(weights[j]) ** 2) * loss_val + np.log(weights[j] ** 2)

            loss_all.backward()
            optim.step()

            train_loss.append(loss_all.item() / len(losses_all))
            train_acc.append(np.mean([acc.item() for acc in acc_all]))

            if iteration_num > 0:
                weights = multiclass_weights_optimize.optimize(weights, losses_all, iteration_num)

                for i, weight in enumerate(weights):
                    weights_traces[i].append(weight)
            # print(weights)

            iteration_num += 1

        # iteration_num += 1

        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

        for i, weight in enumerate(weights):
            weights_traces_epochs[i].append(weight)

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    np.save(os.path.join(opt.experiment_root, 'weights_traces.npy'), np.array(weights_traces))
    np.save(os.path.join(opt.experiment_root, 'weights_traces_epochs.npy'), np.array(weights_traces_epochs))

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_multi_loss_grad_all_loss(opt, tr_dataloader, tr_dataloader_help, model, optim, lr_scheduler, val_dataloader=None,
                                   test_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    iteration_num = 1

    weights = np.array([1 / opt.task_number for _ in range(opt.task_number)])

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        tr_iter_help = iter(tr_dataloader_help)
        model.train()

        for batch in tqdm(tr_iter):
            optim.zero_grad()

            batches = [batch]

            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            losses_all = [loss]
            acc_all = [acc]
            for it in range(1, opt.task_number):
                batch_help = next(tr_iter_help)

                batches.append(batch_help)

                x_help, y_help = batch_help
                x_help, y_help = x_help.to(device), y_help.to(device)
                model_output_help = model(x_help)
                loss_help, acc_help, _ = loss_fn(model_output_help, target=y_help, n_support=opt.num_support_tr, args=opt, is_test=False)

                losses_all.append(loss_help)
                acc_all.append(acc_help)

            loss_all = 0
            for j, loss_val in enumerate(losses_all):
                loss_all += 1 / (float(weights[j]) ** 2) * loss_val + np.log(weights[j] ** 2)
                # loss_all += 1 / (float(weights[j]) ** 4) * loss_val + np.log(weights[j] ** 2)

            loss_all.backward()
            optim.step()

            train_loss.append(loss_all.item() / len(losses_all))
            train_acc.append(np.mean([acc.item() for acc in acc_all]))

            if iteration_num > 0:
                weights = multiclass_weights_optimize.optimize_grad_all_loss(weights, iteration_num, model, batches,
                                                                             loss_fn, device, opt)
            # print(weights)

            iteration_num += 1

        # iteration_num += 1

        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

        # if (epoch + 1) % 5 == 0:
        #     eval(opt)

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_multi_loss_grad_one_loss(opt, tr_dataloader, tr_dataloader_help, model, optim, lr_scheduler, val_dataloader=None,
                          test_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    iteration_num = 1

    weights = np.array([1 / opt.task_number for _ in range(1, opt.task_number)])

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        tr_iter_help = iter(tr_dataloader_help)
        model.train()

        for batch in tqdm(tr_iter):
            optim.zero_grad()

            batches = [batch]

            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            losses_all = [loss]
            acc_all = [acc]
            for it in range(1, opt.task_number):
                batch_help = next(tr_iter_help)

                batches.append(batch_help)

                x_help, y_help = batch_help
                x_help, y_help = x_help.to(device), y_help.to(device)
                model_output_help = model(x_help)
                loss_help, acc_help, _ = loss_fn(model_output_help, target=y_help, n_support=opt.num_support_tr, args=opt, is_test=False)

                losses_all.append(loss_help)
                acc_all.append(acc_help)

            loss_all = losses_all[0]

            loss_all.backward()
            optim.step()

            train_loss.append(loss_all.item() / len(losses_all))
            train_acc.append(np.mean([acc.item() for acc in acc_all]))

            if iteration_num > 0:
                weights = multiclass_weights_optimize.optimize_grad_one_loss(weights, iteration_num, model, batches,
                                                                               loss_fn, device, opt)
            # print(weights)

            iteration_num += 1

        # iteration_num += 1

        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_multi_loss_sampling(opt, tr_dataloader, tr_dataloader_help, model, optim, lr_scheduler, val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    iteration_num = 0

    weights = np.array([1 / opt.top_task_number for _ in range(opt.top_task_number)])

    # epsilon = np.random.normal(loc=0, scale=0.5, size=opt.task_number)
    # weights = np.array([1 / opt.task_number + epsilon[i] for i in range(opt.task_number)])

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        tr_iter_help = iter(tr_dataloader_help)
        model.train()

        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, prototypes = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            losses_all = [loss]
            acc_all = [acc]
            tasks = [prototypes]
            for it in range(1, opt.task_number):
                batch_help = next(tr_iter_help)

                x_help, y_help = batch_help
                x_help, y_help = x_help.to(device), y_help.to(device)
                model_output_help = model(x_help)
                loss_help, acc_help, prototypes_help = loss_fn(model_output_help, target=y_help, n_support=opt.num_support_tr, args=opt, is_test=False)

                # loss_all += float(weights[it]) * loss_help
                losses_all.append(loss_help)
                acc_all.append(acc_help)
                tasks.append(prototypes_help)

            tasks_sample_ind = task_sampling.sample_tasks_indexes(opt, tasks)

            losses_top = []
            for t_ind in tasks_sample_ind:
                losses_top.append(losses_all[t_ind])

            loss_all = 0
            for j, loss_val in enumerate(losses_top):
                loss_all += 1 / (float(weights[j]) ** 2) * loss_val + np.log(weights[j] ** 2)

            loss_all.backward()
            optim.step()

            train_loss.append(loss_all.item() / len(losses_top))
            train_acc.append(np.mean([acc_all[i].item() for i in tasks_sample_ind]))

            if iteration_num > 0:
                weights = multiclass_weights_optimize.optimize(weights, losses_top, iteration_num)
            # print(weights)

            iteration_num += 1

        # iteration_num += 1

        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_multi_loss_sampling_pretrain(opt, tr_dataloader, tr_dataloader_help, model, optims, lr_scheduler, val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    print('=== Pretraining all net ===')

    optim = optims[0]

    for epoch in range(opt.epochs_all):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        model.train()
        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            loss.backward()
            optim.step()
            train_loss.append(loss.item())
            train_acc.append(acc.item())
        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    print('\n=== Training multiloss ===')

    optim = optims[1]

    iteration_num = 0

    weights = np.array([1 / opt.top_task_number for _ in range(opt.top_task_number)])
    # weights = np.array([np.sqrt(opt.task_number) for _ in range(opt.task_number)])

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        tr_iter_help = iter(tr_dataloader_help)
        model.train()

        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, prototypes = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            losses_all = [loss]
            acc_all = [acc]
            tasks = [prototypes]
            for it in range(1, opt.task_number):
                batch_help = next(tr_iter_help)

                x_help, y_help = batch_help
                x_help, y_help = x_help.to(device), y_help.to(device)
                model_output_help = model(x_help)
                loss_help, acc_help, prototypes_help = loss_fn(model_output_help, target=y_help, n_support=opt.num_support_tr, args=opt, is_test=False)

                # loss_all += float(weights[it]) * loss_help
                losses_all.append(loss_help)
                acc_all.append(acc_help)
                tasks.append(prototypes_help)

            tasks_sample_ind = task_sampling.sample_tasks_indexes(opt, tasks)

            losses_top = []
            for t_ind in tasks_sample_ind:
                losses_top.append(losses_all[t_ind])

            loss_all = 0
            for j, loss_val in enumerate(losses_top):
                loss_all += 1 / (float(weights[j]) ** 2) * loss_val + np.log(weights[j] ** 2)

            loss_all.backward()
            optim.step()

            train_loss.append(loss_all.item() / len(losses_top))
            train_acc.append(np.mean([acc_all[i].item() for i in tasks_sample_ind]))

            if iteration_num > 0:
                weights = multiclass_weights_optimize.optimize(weights, losses_top, iteration_num)
            # print(weights)

            iteration_num += 1

        # iteration_num += 1

        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_multi_loss_pretrain_grad_one_loss(opt, tr_dataloader, tr_dataloader_help, model, optims, lr_scheduler,
                                            val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    print('=== Pretraining all net ===')

    optim = optims[0]

    for epoch in range(opt.epochs_all):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        model.train()
        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            loss.backward()
            optim.step()
            train_loss.append(loss.item())
            train_acc.append(acc.item())
        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    print('\n=== Training multiloss grad one ===')

    optim = optims[1]

    iteration_num = 0

    weights = np.array([1 / opt.task_number for _ in range(1, opt.task_number)])

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        tr_iter_help = iter(tr_dataloader_help)
        model.train()

        for batch in tqdm(tr_iter):
            optim.zero_grad()

            batches = [batch]

            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            losses_all = [loss]
            acc_all = [acc]
            for it in range(1, opt.task_number):
                batch_help = next(tr_iter_help)

                batches.append(batch_help)

                x_help, y_help = batch_help
                x_help, y_help = x_help.to(device), y_help.to(device)
                model_output_help = model(x_help)
                loss_help, acc_help, _ = loss_fn(model_output_help, target=y_help, n_support=opt.num_support_tr, args=opt, is_test=False)

                losses_all.append(loss_help)
                acc_all.append(acc_help)

            loss_all = losses_all[0]

            loss_all.backward()
            optim.step()

            train_loss.append(loss_all.item() / len(losses_all))
            train_acc.append(np.mean([acc.item() for acc in acc_all]))

            if iteration_num > 0:
                weights = multiclass_weights_optimize.optimize_grad_one_loss(weights, iteration_num, model, batches,
                                                                               loss_fn, device, opt)
            # print(weights)

            iteration_num += 1

        # iteration_num += 1

        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_multi_loss_track(opt, tr_dataloader, tr_dataloader_help, model, optim, lr_scheduler, val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    iteration_num = 1

    weights = np.array([1 / opt.task_number for _ in range(opt.task_number)])
    # weights = np.array([np.sqrt(opt.task_number) for _ in range(opt.task_number)])

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        tr_iter_help = iter(tr_dataloader_help)
        model.train()

        losses_2n_1 = []
        losses_2n = []

        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            losses_all = [loss]
            acc_all = [acc]
            for it in range(1, opt.task_number):
                batch_help = next(tr_iter_help)

                x_help, y_help = batch_help
                x_help, y_help = x_help.to(device), y_help.to(device)
                model_output_help = model(x_help)
                loss_help, acc_help, _ = loss_fn(model_output_help, target=y_help, n_support=opt.num_support_tr, args=opt, is_test=False)

                losses_all.append(loss_help)
                acc_all.append(acc_help)

            loss_all = 0
            for j, loss_val in enumerate(losses_all):
                loss_all += 1 / (float(weights[j]) ** 2) * loss_val + np.log(weights[j] ** 2)
                # loss_all += 1 / (float(weights[j]) ** 4) * loss_val + np.log(weights[j] ** 2)

            loss_all.backward()
            optim.step()

            train_loss.append(loss_all.item() / len(losses_all))
            train_acc.append(np.mean([acc.item() for acc in acc_all]))

            if not losses_2n_1:
                losses_2n_1 = losses_all
            elif not losses_2n:
                losses_2n = losses_all
            else:
                losses_2n_1 = losses_2n
                losses_2n = losses_all

            if losses_2n_1 and losses_2n:
                weights = multiclass_weights_optimize.optimize_weights_track(weights, (losses_2n_1, losses_2n),
                                                                             iteration_num)
                iteration_num += 1

        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_multi_loss_track_pretrain(opt, tr_dataloader, tr_dataloader_help, model, optims, lr_scheduler, val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    print('=== Pretraining all net ===')

    optim = optims[0]

    for epoch in range(opt.epochs_all):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        model.train()
        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            loss.backward()
            optim.step()
            train_loss.append(loss.item())
            train_acc.append(acc.item())
        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    print('\n=== Training multiloss ===')

    optim = optims[1]

    iteration_num = 1

    weights = np.array([1 / opt.task_number for _ in range(opt.task_number)])
    # weights = np.array([np.sqrt(opt.task_number) for _ in range(opt.task_number)])

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        tr_iter_help = iter(tr_dataloader_help)
        model.train()

        losses_2n_1 = []
        losses_2n = []

        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            losses_all = [loss]
            acc_all = [acc]
            for it in range(1, opt.task_number):
                batch_help = next(tr_iter_help)

                x_help, y_help = batch_help
                x_help, y_help = x_help.to(device), y_help.to(device)
                model_output_help = model(x_help)
                loss_help, acc_help, _ = loss_fn(model_output_help, target=y_help, n_support=opt.num_support_tr, args=opt, is_test=False)

                losses_all.append(loss_help)
                acc_all.append(acc_help)

            loss_all = 0
            for j, loss_val in enumerate(losses_all):
                loss_all += 1 / (float(weights[j]) ** 2) * loss_val + np.log(weights[j] ** 2)
                # loss_all += 1 / (float(weights[j]) ** 4) * loss_val + np.log(weights[j] ** 2)

            loss_all.backward()
            optim.step()

            train_loss.append(loss_all.item() / len(losses_all))
            train_acc.append(np.mean([acc.item() for acc in acc_all]))

            if not losses_2n_1:
                losses_2n_1 = losses_all
            elif not losses_2n:
                losses_2n = losses_all
            else:
                losses_2n_1 = losses_2n
                losses_2n = losses_all

            if losses_2n_1 and losses_2n:
                weights = multiclass_weights_optimize.optimize_weights_track(weights, (losses_2n_1, losses_2n),
                                                                             iteration_num)
                iteration_num += 1

        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def train_track(opt, tr_dataloader, model, optims, lr_scheduler, val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).replace(' ', '_')
    model_prefix = '{}_{}'.format(opt.omniglot_type, time_str)
    model_dir = os.path.join(opt.experiment_root, model_prefix)
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)

    config_file = os.path.join(model_dir, 'train_config.txt')
    with open(config_file, 'w') as f:
        f.write(str(opt))

    best_model_path = os.path.join(model_dir, 'best_model.pth')
    last_model_path = os.path.join(model_dir, 'last_model.pth')

    print('=== Training all net ===')

    optim = optims[0]

    for epoch in range(opt.epochs_all):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        model.train()
        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=False)

            loss.backward()
            optim.step()
            train_loss.append(loss.item())
            train_acc.append(acc.item())
        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    print('\n=== Training only last layer ===')

    spsa_gamma = 1. / 6
    spsa_alpha = lambda x: 0.25 / (x ** spsa_gamma)
    spsa_beta = lambda x: 15. / (x ** (spsa_gamma / 4))

    # spsa_alpha = lambda x: 0.001
    # spsa_beta = lambda x: 0.001

    tracking = Tracking(model, loss_fn, alpha=spsa_alpha, beta=spsa_beta, latent_size=8)

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        model.train()
        for batch_2n_1 in tqdm(tr_iter):
            batch_2n = next(tr_iter)

            tracking.optimize(model, batch_2n, batch_2n_1, device, opt)

            train_loss.append(loss.item())
            train_acc.append(acc.item())
        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {0:.4f}, Avg Train Acc: {1:.4f}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            loss, acc, _ = loss_fn(model_output, target=y, n_support=opt.num_support_val, args=opt, is_test=True)

            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {0:.4f})'.format(
            best_acc)
        print('Avg Val Loss: {0:.4f}, Avg Val Acc: {1:.4f}{2}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root, name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def test(opt, test_dataloader, model):
    '''
    Test the model trained with the prototypical learning algorithm
    '''
    device = 'cuda:{}'.format(opt.device) if torch.cuda.is_available() and opt.cuda else 'cpu'
    avg_acc = list()
    for epoch in range(10):
        test_iter = iter(test_dataloader)
        for batch in test_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)

            _, acc, __ = loss_fn(model_output, target=y, n_support=opt.num_support_tr, args=opt, is_test=True)

            avg_acc.append(acc.item())
    mean_acc = np.mean(avg_acc)
    std_acc = np.std(avg_acc)
    conf_int = 1.962 * std_acc / np.sqrt(10 * opt.iterations)
    print('Test Acc: {0:.4f}, Conf int: {1:.4f}'.format(mean_acc, conf_int))

    return avg_acc


def eval(opt):
    '''
    Initialize everything and train
    '''
    options = get_parser().parse_args()

    if torch.cuda.is_available() and not options.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    init_seed(options)
    test_dataloader = init_dataset(options)[-1]
    model = init_protonet(options)
    model_path = os.path.join(opt.experiment_root, 'best_model.pth')
    model.load_state_dict(torch.load(model_path))

    test(opt=options,
         test_dataloader=test_dataloader,
         model=model)


def main():
    '''
    Initialize everything and train
    '''
    options = get_parser().parse_args()
    if not os.path.exists(options.experiment_root):
        os.makedirs(options.experiment_root)

    if torch.cuda.is_available() and not options.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    if options.eval:
        print('Evaluating with best model..')
        eval(options)
        return

    init_seed(options)
    # tr_dataloader, val_dataloader, trainval_dataloader, test_dataloader = init_dataset(options)


    if options.multiloss or options.multiloss_pretrain or options.multiloss_grad_all or options.multiloss_grad_one \
            or options.multiloss_sampling or options.multiloss_pretrain_grad_one or options.multiloss_track or \
            options.multiloss_track_pretrain or options.multiloss_sampling_pretrain:
        tr_dataloader, val_dataloader, test_dataloader, tr_dataloader_help = init_dataset(options)
    else:
        tr_dataloader, val_dataloader, test_dataloader = init_dataset(options)
    model = init_protonet(options)
    optim, optim_next = init_optim(options, model)
    lr_scheduler = init_lr_scheduler(options, optim)

    print()
    print('Training: classes per episode: {}, support set num: {}, query set num: {}'.format(options.classes_per_it_tr,
                                                                                             options.num_support_tr,
                                                                                             options.num_query_tr))
    print('Val: classes per episode: {}, support set num: {}, query set num: {}'.format(options.classes_per_it_val,
                                                                                        options.num_support_val,
                                                                                        options.num_query_val))

    if options.spsa or options.all_spsa:
        print('\n=== Using SPSA prototypes ===\n')
    elif options.sps:
        print('\n=== Using SPS ===\n')

    if options.multiloss:
        print('\n=== Using Multiloss ===\n')
        # res = train_multi_loss(opt=options,
        #             tr_dataloader=tr_dataloader,
        #             val_dataloader=val_dataloader,
        #             tr_dataloader_help = tr_dataloader_help,
        #             model=model,
        #             optim=optim,
        #             lr_scheduler=lr_scheduler)
        res = train_multi_loss(opt=options,
                               tr_dataloader=tr_dataloader,
                               val_dataloader=test_dataloader,
                               tr_dataloader_help=tr_dataloader_help,
                               model=model,
                               optim=optim,
                               lr_scheduler=lr_scheduler)
    elif options.multiloss_pretrain:
        print('\n=== Using Multiloss pretraining ===\n')
        res = train_multi_loss_pretrain(opt=options,
                               tr_dataloader=tr_dataloader,
                               val_dataloader=test_dataloader,
                               tr_dataloader_help=tr_dataloader_help,
                               model=model,
                               optims=(optim, optim_next),
                               lr_scheduler=lr_scheduler)
    elif options.multiloss_grad_all:
        print('\n=== Using Multiloss Grad All ===\n')
        # res = train_multi_loss_grad(opt=options,
        #                            tr_dataloader=tr_dataloader,
        #                            val_dataloader=val_dataloader,
        #                            tr_dataloader_help=tr_dataloader_help,
        #                            test_dataloader=test_dataloader,
        #                            model=model,
        #                            optim=optim,
        #                            lr_scheduler=lr_scheduler)
        res = train_multi_loss_grad_all_loss(opt=options,
                                             tr_dataloader=tr_dataloader,
                                             val_dataloader=test_dataloader,
                                             tr_dataloader_help=tr_dataloader_help,
                                             test_dataloader=test_dataloader,
                                             model=model,
                                             optim=optim,
                                             lr_scheduler=lr_scheduler)
    elif options.multiloss_grad_one:
        print('\n=== Using Multiloss Grad One ===\n')
        # res = train_multi_loss_grad_one_loss(opt=options,
        #                            tr_dataloader=tr_dataloader,
        #                            val_dataloader=val_dataloader,
        #                            tr_dataloader_help=tr_dataloader_help,
        #                            test_dataloader=test_dataloader,
        #                            model=model,
        #                            optim=optim,
        #                            lr_scheduler=lr_scheduler)
        res = train_multi_loss_grad_one_loss(opt=options,
                                    tr_dataloader=tr_dataloader,
                                    val_dataloader=test_dataloader,
                                    tr_dataloader_help=tr_dataloader_help,
                                    test_dataloader=test_dataloader,
                                    model=model,
                                    optim=optim,
                                    lr_scheduler=lr_scheduler)
    elif options.multiloss_sampling:
        print('\n=== Using Multiloss With Sampling ===\n')
        # res = train_multi_loss_sampling(opt=options,
        #                            tr_dataloader=tr_dataloader,
        #                            val_dataloader=val_dataloader,
        #                            tr_dataloader_help=tr_dataloader_help,
        #                            model=model,
        #                            optim=optim,
        #                            lr_scheduler=lr_scheduler)
        res = train_multi_loss_sampling(opt=options,
                                    tr_dataloader=tr_dataloader,
                                    val_dataloader=test_dataloader,
                                    tr_dataloader_help=tr_dataloader_help,
                                    model=model,
                                    optim=optim,
                                    lr_scheduler=lr_scheduler)
    elif options.multiloss_pretrain_grad_one:
        print('\n=== Using Multiloss pretraining with grad one===\n')
        res = train_multi_loss_pretrain_grad_one_loss(opt=options,
                                                      tr_dataloader=tr_dataloader,
                                                      val_dataloader=test_dataloader,
                                                      tr_dataloader_help=tr_dataloader_help,
                                                      model=model,
                                                      optims=(optim, optim_next),
                                                      lr_scheduler=lr_scheduler)
    elif options.multiloss_track:
        print('\n=== Using Multiloss with tracking===\n')
        res = train_multi_loss_track(opt=options,
                                     tr_dataloader=tr_dataloader,
                                     val_dataloader=test_dataloader,
                                     tr_dataloader_help=tr_dataloader_help,
                                     model=model,
                                     optim=optim,
                                     lr_scheduler=lr_scheduler)
    elif options.multiloss_track_pretrain:
        print('\n=== Using Multiloss with tracking pretraining ===\n')
        res = train_multi_loss_track_pretrain(opt=options,
                                              tr_dataloader=tr_dataloader,
                                              val_dataloader=test_dataloader,
                                              tr_dataloader_help=tr_dataloader_help,
                                              model=model,
                                              optims=(optim, optim_next),
                                              lr_scheduler=lr_scheduler)
    elif options.multiloss_sampling_pretrain:
        print('\n=== Using Multiloss with sampling pretraining ===\n')
        res = train_multi_loss_sampling_pretrain(opt=options,
                                                 tr_dataloader=tr_dataloader,
                                                 val_dataloader=test_dataloader,
                                                 tr_dataloader_help=tr_dataloader_help,
                                                 model=model,
                                                 optims=(optim, optim_next),
                                                 lr_scheduler=lr_scheduler)
    elif options.tracking:
        print('\n=== Using Tracking ===\n')
        res = train_track(opt=options,
                       tr_dataloader=tr_dataloader,
                       val_dataloader=val_dataloader,
                       model=model,
                       optims=(optim, optim_next),
                       lr_scheduler=lr_scheduler)
    else:
        res = train(opt=options,
                    tr_dataloader=tr_dataloader,
                    val_dataloader=val_dataloader,
                    model=model,
                    optim=optim,
                    lr_scheduler=lr_scheduler)

    best_state, best_acc, train_loss, train_acc, val_loss, val_acc = res
    print('Testing with last model..')
    test(opt=options,
         test_dataloader=test_dataloader,
         model=model)

    model.load_state_dict(best_state)
    print('Testing with best model..')
    test(opt=options,
         test_dataloader=test_dataloader,
         model=model)

    # optim = init_optim(options, model)
    # lr_scheduler = init_lr_scheduler(options, optim)

    # print('Training on train+val set..')
    # train(opt=options,
    #       tr_dataloader=trainval_dataloader,
    #       val_dataloader=None,
    #       model=model,
    #       optim=optim,
    #       lr_scheduler=lr_scheduler)

    # print('Testing final model..')
    # test(opt=options,
    #      test_dataloader=test_dataloader,
    #      model=model)


if __name__ == '__main__':
    main()
