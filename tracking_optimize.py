# coding=utf-8
import numpy as np
import copy
import torch


# spsa_alpha = lambda x: 0.001
# spsa_beta = lambda x: 0.001

spsa_gamma = 1. / 6
spsa_alpha = lambda x: 0.25 / (x ** spsa_gamma)
spsa_beta = lambda x: 15. / (x ** (spsa_gamma / 4))


class Tracking(object):

    def __init__(self, net, loss_fn, alpha=lambda x: 0.001, beta=lambda x: 0.001, latent_size=32):
        w = net.fc.weight.clone()
        self.theta_2n_2 = w.view(-1).detach().cpu().numpy()

        self.loss_fn = loss_fn

        self.alpha = alpha
        self.beta = beta

        self.latent_size = latent_size

        self.iteration_num = 1

    def delta_fabric(self, d):
        return np.where(np.random.binomial(1, 0.5, size=d) == 0, -1, 1)
        # return np.random.binomial(1, 0.5, size=d)

    def alpha_fabric(self):
        return self.alpha(self.iteration_num)

    def beta_fabric(self):
        return self.beta(self.iteration_num)

    def optimize(self, net, batch_2n, batch_2n_1, device, opt):
        w = net.fc.parameters()
        param = list(w)
        theta = param[0].view(-1).detach().cpu().numpy()

        # print(param[0].size())

        delta_n = self.delta_fabric(theta.shape[0])
        alpha_n = self.alpha_fabric()
        beta_n = self.beta_fabric()

        x_2n = self.theta_2n_2 + beta_n * delta_n
        x_2n_1 = self.theta_2n_2 - beta_n * delta_n

        theta_2n_1 = self.theta_2n_2

        net_2n = copy.deepcopy(net)
        net_2n_1 = copy.deepcopy(net)

        net_2n.fc.weight = torch.nn.Parameter(torch.from_numpy(x_2n.reshape(self.latent_size, 64)).float())
        net_2n_1.fc.weight = torch.nn.Parameter(torch.from_numpy(x_2n_1.reshape(self.latent_size, 64)).float())

        net_2n.to(device)
        net_2n_1.to(device)

        inp_2n, out_2n = batch_2n
        inp_2n, out_2n = inp_2n.to(device), out_2n.to(device)
        model_output_2n = net_2n(inp_2n)
        y_2n, _, __ = self.loss_fn(model_output_2n, target=out_2n, n_support=opt.num_support_tr, args=opt, is_test=False)

        inp_2n_1, out_2n_1 = batch_2n_1
        inp_2n_1, out_2n_1 = inp_2n_1.to(device), out_2n_1.to(device)
        model_output_2n_1 = net_2n_1(inp_2n_1)
        y_2n_1, _, __ = self.loss_fn(model_output_2n_1, target=out_2n_1, n_support=opt.num_support_tr, args=opt, is_test=False)

        theta_2n = theta_2n_1 - alpha_n * delta_n * (y_2n.detach().numpy() - y_2n_1.detach().numpy()) / beta_n
        # theta_2n = theta_2n_1 - alpha_n * delta_n * (y_2n.detach().numpy() - y_2n_1.detach().numpy()) / (2 * beta_n)

        net.fc.weight = torch.nn.Parameter(torch.from_numpy(theta_2n.reshape(self.latent_size, 64)).float().to(device))

        self.iteration_num += 1

