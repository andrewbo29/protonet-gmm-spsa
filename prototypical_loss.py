# coding=utf-8
import torch
from torch.nn import functional as F
from torch.nn.modules import Module
# from torch.nn.modules.loss import _assert_no_grad
import numpy as np
import time

from spsa_clustering import ClusteringSPSA
from sps_experiment import sps_estimate


class PrototypicalLoss(Module):
    '''
    Loss class deriving from Module for the prototypical loss function defined below
    '''

    def __init__(self, n_support, is_spsa=False):
        super(PrototypicalLoss, self).__init__()
        self.n_support = n_support
        self.is_spsa = is_spsa

    def forward(self, input, target):
        # _assert_no_grad(target)
        return prototypical_loss(input, target, self.n_support, self.is_spsa)


def euclidean_dist(x, y):
    '''
    Compute euclidean distance between two tensors
    '''
    # x: N x D
    # y: M x D
    n = x.size(0)
    m = y.size(0)
    d = x.size(1)
    if d != y.size(1):
        raise Exception

    x = x.unsqueeze(1).expand(n, m, d)
    y = y.unsqueeze(0).expand(n, m, d)

    return torch.pow(x - y, 2).sum(2)


def spsa_prototypes_calculate(inputs, support_idxs, query_idxs, args):
    n_classes = len(support_idxs)

    spsa_alpha = lambda x: 0.001
    spsa_beta = lambda x: 0.001

    # spsa_gamma = 1. / 6
    # spsa_alpha = lambda x: 0.25 / (x ** spsa_gamma)
    # spsa_beta = lambda x: 15. / (x ** (spsa_gamma / 4))

    spsa = ClusteringSPSA(n_clusters=n_classes, data_shape=inputs.size()[1], verbose=False, alpha=spsa_alpha,
                          beta=spsa_beta, sparse=args.sparse)

    support_indexes = []
    # true_labels = []
    # data_set = []

    for i in range(n_classes):
        init_ind = support_idxs[i][0]
        support_indexes.extend(support_idxs[i][1:].numpy().tolist().copy())
        x = inputs[init_ind].detach().cpu().numpy()
        spsa.fit(x)

        # true_labels.extend([i] * len(support_idxs[i][1:]))
        # for j in range(len(support_idxs[i][1:])):
        #     data_set.append(inputs[j].detach().cpu().numpy())
    # data_set = np.array(data_set)

    query_indexes = query_idxs.numpy().tolist().copy()

    np.random.shuffle(support_indexes)
    np.random.shuffle(query_indexes)

    for _ in range(1):
        # np.random.shuffle(support_indexes)
        for i in support_indexes:
            spsa.fit(inputs[i].detach().numpy())

            # spsa.clusters_fill(data_set)
            # ari_spsa = metrics.adjusted_rand_score(true_labels, spsa.labels_)
            # print('Ari support set: {}'.format(ari_spsa))

        for j in query_indexes:
            spsa.fit(inputs[j].detach().numpy())

    spsa.cluster_centers_ = np.array(spsa.cluster_centers_)

    return spsa.cluster_centers_


def sps_prototypes_calculate(inputs, support_idxs, query_idxs):
    n_classes = len(support_idxs)

    sps_proto = []
    for i in range(n_classes):
        support_set = inputs[support_idxs[i]].detach().cpu().numpy()
        query_set = inputs[query_idxs[i]].detach().cpu().numpy()

        q = 3
        M = 10

        sps_proto_class = sps_estimate(support_set, query_set, q=q, M=M, max_num=10)
        sps_proto.append(sps_proto_class)

    return np.array(sps_proto)


def prototypical_loss(input, target, n_support, args, is_test=False):
    '''
    Inspired by https://github.com/jakesnell/prototypical-networks/blob/master/protonets/models/few_shot.py

    Compute the barycentres by averaging the features of n_support
    samples for each class in target, computes then the distances from each
    samples' features to each one of the barycentres, computes the
    log_probability for each n_query samples for each one of the current
    classes, of appartaining to a class c, loss and accuracy are then computed
    and returned
    Args:
    - input: the model output for a batch of samples
    - target: ground truth for the above batch of samples
    - n_support: number of samples to keep in account when computing
      barycentres, for each one of the current classes
    '''
    target_cpu = target.to('cpu')
    input_cpu = input.to('cpu')

    def supp_idxs(c):
        # FIXME when torch will support where as np
        return target_cpu.eq(c).nonzero()[:n_support].squeeze()

    # FIXME when torch.unique will be available on cuda too
    classes = torch.unique(target_cpu)
    n_classes = len(classes)
    # FIXME when torch will support where as np
    # assuming n_query, n_target constants
    n_query = target_cpu.eq(classes[0].item()).sum().item() - n_support

    support_idxs = list(map(supp_idxs, classes))

    # FIXME when torch will support where as np
    query_idxs = torch.stack(list(map(lambda c: target_cpu.eq(c).nonzero()[n_support:], classes))).view(-1)

    if is_test and not args.all_spsa:
        # one-shot
        if len(input_cpu[support_idxs[0]].size()) == 1:
            prototypes = torch.stack([input_cpu[i] for i in support_idxs])
        else:
            prototypes = torch.stack([input_cpu[i].mean(0) for i in support_idxs])
    elif is_test and args.spsa_test:
        spsa_prototypes = spsa_prototypes_calculate(input_cpu, support_idxs, query_idxs, args)
        prototypes = torch.from_numpy(spsa_prototypes)
    else:
        if args.spsa or args.all_spsa:
            spsa_prototypes = spsa_prototypes_calculate(input_cpu, support_idxs, query_idxs, args)

            if args.reg_spsa:
                spsa_prototypes = torch.from_numpy(spsa_prototypes)
                prototypes = torch.stack([input_cpu[i].mean(0) for i in support_idxs])
            else:
                prototypes = torch.from_numpy(spsa_prototypes)

        elif args.sps:
            sps_prototypes = sps_prototypes_calculate(input_cpu, support_idxs, query_idxs)
            sps_prototypes = torch.from_numpy(sps_prototypes)
            sps_prototypes = sps_prototypes.float()

            # one-shot
            if len(input_cpu[support_idxs[0]].size()) == 1:
                prototypes = torch.stack([input_cpu[i] for i in support_idxs])
            else:
                prototypes = torch.stack([input_cpu[i].mean(0) for i in support_idxs])
        else:
            # one-shot
            if len(input_cpu[support_idxs[0]].size()) == 1:
                prototypes = torch.stack([input_cpu[i] for i in support_idxs])
            else:
                prototypes = torch.stack([input_cpu[i].mean(0) for i in support_idxs])

    query_samples = input.to('cpu')[query_idxs]
    dists = euclidean_dist(query_samples, prototypes)

    log_p_y = F.log_softmax(-dists, dim=1).view(n_classes, n_query, -1)

    target_inds = torch.arange(0, n_classes)
    target_inds = target_inds.view(n_classes, 1, 1)
    target_inds = target_inds.expand(n_classes, n_query, 1).long()

    loss_val = -log_p_y.gather(2, target_inds).squeeze().view(-1).mean()
    _, y_hat = log_p_y.max(2)
    acc_val = y_hat.eq(target_inds.squeeze()).float().mean()

    if args.sps and not is_test:
        dist_lr = 1e-3
        dist = torch.pow(prototypes - sps_prototypes, 2).reshape(-1).sum()
        loss_val += dist_lr * dist

    if args.reg_spsa and not is_test:
        dist_lr = 1e-3
        dist = torch.pow(prototypes - spsa_prototypes, 2).reshape(-1).sum()
        loss_val += dist_lr * dist

    # return loss_val, acc_val
    return loss_val, acc_val, prototypes


def prototypical_loss_soft_nn(inputs, target, n_support, args, epoch):
    '''
    Inspired by https://github.com/jakesnell/prototypical-networks/blob/master/protonets/models/few_shot.py

    Compute the barycentres by averaging the features of n_support
    samples for each class in target, computes then the distances from each
    samples' features to each one of the barycentres, computes the
    log_probability for each n_query samples for each one of the current
    classes, of appartaining to a class c, loss and accuracy are then computed
    and returned
    Args:
    - input: the model output for a batch of samples
    - target: ground truth for the above batch of samples
    - n_support: number of samples to keep in account when computing
      barycentres, for each one of the current classes
    '''
    target_cpu = target.to('cpu')

    inputs_cpu = []
    for inp in inputs:
        # print(inp.size())
        inputs_cpu.append(inp.to('cpu'))

    def supp_idxs(c):
        # FIXME when torch will support where as np
        return target_cpu.eq(c).nonzero()[:n_support].squeeze()

    # FIXME when torch.unique will be available on cuda too
    classes = torch.unique(target_cpu)
    n_classes = len(classes)
    # FIXME when torch will support where as np
    # assuming n_query, n_target constants
    n_query = target_cpu.eq(classes[0].item()).sum().item() - n_support

    support_idxs = list(map(supp_idxs, classes))

    # FIXME when torch will support where as np
    query_idxs = torch.stack(list(map(lambda c: target_cpu.eq(c).nonzero()[n_support:], classes))).view(-1)

    prototypes = []
    for i, input_cpu in enumerate(inputs_cpu):
        # one-shot
        if len(input_cpu[support_idxs[0]].size()) == 1:
            prototypes.append(torch.stack([input_cpu[i] for i in support_idxs]))
        else:
            if args.spsa:
                spsa_prototypes = spsa_prototypes_calculate(input_cpu, support_idxs, query_idxs, args)
                prototypes.append(torch.from_numpy(spsa_prototypes))
            else:
                prototypes.append(torch.stack([input_cpu[i].mean(0) for i in support_idxs]))

    target_inds = torch.arange(0, n_classes)
    target_inds = target_inds.view(n_classes, 1, 1)
    target_inds = target_inds.expand(n_classes, n_query, 1).long()

    log_p_y_list = []

    temp = np.exp(-0.1 * epoch) * args.temp

    alpha = args.alpha - epoch // 10

    for i, inp in enumerate(inputs):
        query_samples = inp.to('cpu')[query_idxs]

        # support_samples = inp.to('cpu')[torch.cat(support_idxs)]

        if i < len(inputs) - 1:
            dists = euclidean_dist(query_samples, prototypes[i]) / temp
            # dists = euclidean_dist(query_samples, support_samples) / args.temp
            # log_p_y = F.log_softmax(-dists, dim=1).view(n_support, n_query, -1)
        else:
            dists = euclidean_dist(query_samples, prototypes[i])
            # log_p_y = F.log_softmax(-dists, dim=1).view(n_classes, n_query, -1)

        log_p_y = F.log_softmax(-dists, dim=1).view(n_classes, n_query, -1)

        log_p_y_list.append(log_p_y)

    reg_loss = 0
    for log_p_y in log_p_y_list[:-1]:
        reg_loss += -log_p_y.gather(2, target_inds).squeeze().view(-1).mean()

    loss_val = -log_p_y_list[-1].gather(2, target_inds).squeeze().view(-1).mean() + alpha * reg_loss

    _, y_hat = log_p_y_list[-1].max(2)
    acc_val = y_hat.eq(target_inds.squeeze()).float().mean()

    return loss_val, acc_val


def prototypical_loss_scale(input, target, n_support, args, alpha, is_test=False):
    '''
    Inspired by https://github.com/jakesnell/prototypical-networks/blob/master/protonets/models/few_shot.py

    Compute the barycentres by averaging the features of n_support
    samples for each class in target, computes then the distances from each
    samples' features to each one of the barycentres, computes the
    log_probability for each n_query samples for each one of the current
    classes, of appartaining to a class c, loss and accuracy are then computed
    and returned
    Args:
    - input: the model output for a batch of samples
    - target: ground truth for the above batch of samples
    - n_support: number of samples to keep in account when computing
      barycentres, for each one of the current classes
    '''
    target_cpu = target.to('cpu')
    input_cpu = input.to('cpu')

    def supp_idxs(c):
        # FIXME when torch will support where as np
        return target_cpu.eq(c).nonzero()[:n_support].squeeze()

    # FIXME when torch.unique will be available on cuda too
    classes = torch.unique(target_cpu)
    n_classes = len(classes)
    # FIXME when torch will support where as np
    # assuming n_query, n_target constants
    n_query = target_cpu.eq(classes[0].item()).sum().item() - n_support

    support_idxs = list(map(supp_idxs, classes))

    # FIXME when torch will support where as np
    query_idxs = torch.stack(list(map(lambda c: target_cpu.eq(c).nonzero()[n_support:], classes))).view(-1)

    if is_test and not args.all_spsa:
        # one-shot
        if len(input_cpu[support_idxs[0]].size()) == 1:
            prototypes = torch.stack([input_cpu[i] for i in support_idxs])
        else:
            prototypes = torch.stack([input_cpu[i].mean(0) for i in support_idxs])
    else:
        if args.spsa:
            spsa_prototypes = spsa_prototypes_calculate(input_cpu, support_idxs, query_idxs, args)

            if args.reg_spsa:
                spsa_prototypes = torch.from_numpy(spsa_prototypes)
                prototypes = torch.stack([input_cpu[i].mean(0) for i in support_idxs])
            else:
                prototypes = torch.from_numpy(spsa_prototypes)

        elif args.sps:
            sps_prototypes = sps_prototypes_calculate(input_cpu, support_idxs, query_idxs)
            sps_prototypes = torch.from_numpy(sps_prototypes)
            sps_prototypes = sps_prototypes.float()

            # one-shot
            if len(input_cpu[support_idxs[0]].size()) == 1:
                prototypes = torch.stack([input_cpu[i] for i in support_idxs])
            else:
                prototypes = torch.stack([input_cpu[i].mean(0) for i in support_idxs])
        else:
            # one-shot
            if len(input_cpu[support_idxs[0]].size()) == 1:
                prototypes = torch.stack([input_cpu[i] for i in support_idxs])
            else:
                prototypes = torch.stack([input_cpu[i].mean(0) for i in support_idxs])

    query_samples = input.to('cpu')[query_idxs]
    dists = float(alpha) * euclidean_dist(query_samples, prototypes)

    log_p_y = F.log_softmax(-dists, dim=1).view(n_classes, n_query, -1)

    target_inds = torch.arange(0, n_classes)
    target_inds = target_inds.view(n_classes, 1, 1)
    target_inds = target_inds.expand(n_classes, n_query, 1).long()

    loss_val = -log_p_y.gather(2, target_inds).squeeze().view(-1).mean()
    _, y_hat = log_p_y.max(2)
    acc_val = y_hat.eq(target_inds.squeeze()).float().mean()

    if args.sps and not is_test:
        dist_lr = 1e-3
        dist = torch.pow(prototypes - sps_prototypes, 2).reshape(-1).sum()
        loss_val += dist_lr * dist

    if args.reg_spsa and not is_test:
        dist_lr = 1e-3
        dist = torch.pow(prototypes - spsa_prototypes, 2).reshape(-1).sum()
        loss_val += dist_lr * dist

    # return loss_val, acc_val
    return loss_val, acc_val, prototypes
