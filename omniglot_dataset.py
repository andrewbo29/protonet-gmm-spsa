# coding=utf-8
from __future__ import print_function
import torch.utils.data as data
import numpy as np
import errno
import os
from PIL import Image
import torch
import shutil
from collections import defaultdict

'''
Inspired by https://github.com/pytorch/vision/pull/46
'''

IMG_CACHE = {}


class OmniglotDataset(data.Dataset):
    vinalys_baseurl = 'https://raw.githubusercontent.com/jakesnell/prototypical-networks/master/data/omniglot/splits/vinyals/'
    vinyals_split_sizes = {
        'test': vinalys_baseurl + 'test.txt',
        'train': vinalys_baseurl + 'train.txt',
        'trainval': vinalys_baseurl + 'trainval.txt',
        'val': vinalys_baseurl + 'val.txt',
    }

    urls = [
        'https://github.com/brendenlake/omniglot/raw/master/python/images_background.zip',
        'https://github.com/brendenlake/omniglot/raw/master/python/images_evaluation.zip'
    ]
    splits_folder = os.path.join('splits', 'vinyals')
    raw_folder = 'raw'
    processed_folder = 'data'

    def __init__(self, mode='train', root='../dataset', exp_type='augment_between', transform=None, target_transform=None, download=False):
        '''
        The items are (filename,category). The index of all the categories can be found in self.idx_classes
        Args:
        - root: the directory where the dataset will be stored
        - transform: how to transform the input
        - target_transform: how to transform the target
        - download: need to download the dataset
        - exp_type: type of experiment
        '''
        super(OmniglotDataset, self).__init__()
        self.root = root
        self.transform = transform
        self.target_transform = target_transform
        self.exp_type = exp_type

        if self.exp_type == 'augment_between':
            print('\n== Experiment type: Augmented Between alphabets')
        elif self.exp_type == 'original_between':
            print('\n== Experiment type: Original Between alphabets')
        elif self.exp_type == 'augment_within':
            print('\n== Experiment type: Augmented Within alphabet')
        elif self.exp_type == 'original_within':
            print('\n== Experiment type: Original Within alphabet')

        print('\n== Dataset mode: {}'.format(mode))

        if download:
            self.download()

        if not self._check_exists():
            raise RuntimeError(
                'Dataset not found. You can use download=True to download it')

        if self.exp_type.startswith('original'):
            self.classes = get_current_classes_orig(os.path.join(self.root, self.splits_folder, mode + '.txt'))

            self.all_items = find_items_orig(os.path.join(self.root, self.processed_folder), self.classes)
        else:
            self.classes = get_current_classes(os.path.join(self.root, self.splits_folder, mode + '.txt'))

            self.all_items = find_items(os.path.join(self.root, self.processed_folder), self.classes)

        self.idx_classes = index_classes(self.all_items)

        paths, self.y, alphabets_list = zip(*[self.get_path_label(pl) for pl in range(len(self))])

        self.x = map(load_img, paths, range(len(paths)))
        self.x = list(self.x)

        self.alphabets = defaultdict(set)
        self.get_alphabets(alphabets_list)

        # for al in self.alphabets:
        #     print(len(self.alphabets[al]))

    def __getitem__(self, idx):
        x = self.x[idx]
        if self.transform:
            x = self.transform(x)
        return x, self.y[idx]

    def __len__(self):
        return len(self.all_items)

    def get_path_label(self, index):
        filename = self.all_items[index][0]
        rot = self.all_items[index][-1]
        img = str.join('/', [self.all_items[index][2], filename]) + rot
        target = self.idx_classes[self.all_items[index][1] + self.all_items[index][-1]]

        if self.target_transform is not None:
            target = self.target_transform(target)

        albph = self.all_items[index][1].split('/')[0]

        return img, target, albph

    def _check_exists(self):
        return os.path.exists(os.path.join(self.root, self.processed_folder))

    def download(self):
        from six.moves import urllib
        import zipfile

        if self._check_exists():
            return

        try:
            os.makedirs(os.path.join(self.root, self.splits_folder))
            os.makedirs(os.path.join(self.root, self.raw_folder))
            os.makedirs(os.path.join(self.root, self.processed_folder))
        except OSError as e:
            if e.errno == errno.EEXIST:
                pass
            else:
                raise

        for k, url in self.vinyals_split_sizes.items():
            print('== Downloading ' + url)
            data = urllib.request.urlopen(url)
            filename = url.rpartition('/')[-1]
            file_path = os.path.join(self.root, self.splits_folder, filename)
            with open(file_path, 'wb') as f:
                f.write(data.read())

        for url in self.urls:
            print('== Downloading ' + url)
            data = urllib.request.urlopen(url)
            filename = url.rpartition('/')[2]
            file_path = os.path.join(self.root, self.raw_folder, filename)
            with open(file_path, 'wb') as f:
                f.write(data.read())
            orig_root = os.path.join(self.root, self.raw_folder)
            print("== Unzip from " + file_path + " to " + orig_root)
            zip_ref = zipfile.ZipFile(file_path, 'r')
            zip_ref.extractall(orig_root)
            zip_ref.close()
        file_processed = os.path.join(self.root, self.processed_folder)
        for p in ['images_background', 'images_evaluation']:
            for f in os.listdir(os.path.join(orig_root, p)):
                shutil.move(os.path.join(orig_root, p, f), file_processed)
            os.rmdir(os.path.join(orig_root, p))
        print("Download finished.")

    def get_alphabets(self, alphab_list):
        for i, y in enumerate(self.y):
            self.alphabets[alphab_list[i]].add(y)

        print("== Dataset: Found %d alphabets" % len(self.alphabets))


def find_items(root_dir, classes):
    retour = []
    rots = ['/rot000', '/rot090', '/rot180', '/rot270']
    for (root, dirs, files) in os.walk(root_dir):
        for f in files:
            r = root.split('/')
            lr = len(r)
            label = r[lr - 2] + "/" + r[lr - 1]
            for rot in rots:
                if label + rot in classes and (f.endswith("png")):
                    retour.extend([(f, label, root, rot)])
    print("== Dataset: Found %d items " % len(retour))
    return retour


def find_items_orig(root_dir, classes):
    retour = []
    rot = '/rot000'
    for (root, dirs, files) in os.walk(root_dir):
        for f in files:
            r = root.split('/')
            lr = len(r)
            label = r[lr - 2] + "/" + r[lr - 1]
            if label in classes and (f.endswith("png")):
                retour.extend([(f, label, root, rot)])
    print("== Dataset: Found %d items " % len(retour))
    return retour


def index_classes(items):
    idx = {}
    for i in items:
        if (not i[1] + i[-1] in idx):
            idx[i[1] + i[-1]] = len(idx)
    print("== Dataset: Found %d classes" % len(idx))
    return idx


def get_current_classes(fname):
    with open(fname) as f:
        classes = f.read().splitlines()
    return classes


def get_current_classes_orig(fname):
    classes = []
    with open(fname) as f:
        for line in f:
            split_res = line.rstrip('/n').split('/')
            classes.append('/'.join(split_res[:-1]))
    return set(classes)


def load_img(path, idx):
    path, rot = path.split('/rot')
    if path in IMG_CACHE:
        x = IMG_CACHE[path]
    else:
        x = Image.open(path)
        IMG_CACHE[path] = x
    x = x.rotate(float(rot))
    x = x.resize((28, 28))

    shape = 1, x.size[0], x.size[1]
    x = np.array(x, np.float32, copy=False)
    x = 1.0 - torch.from_numpy(x)
    x = x.transpose(0, 1).contiguous().view(shape)

    return x
