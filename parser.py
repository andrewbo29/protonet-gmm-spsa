# coding=utf-8
import argparse


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-root', '--dataset_root',
                        type=str,
                        help='path to dataset',
                        default='../dataset')

    parser.add_argument('-dataset',
                        type=str,
                        default='omniglot',
                        help='dataset type: omniglot, mini-imagenet, tiered-imagenet')

    parser.add_argument('-omniglot_type',
                        type=str,
                        default='augment_between',
                        help='experiment type for omniglot: augment_between, original_between, '
                             'augment_within, original_within')

    parser.add_argument('-exp', '--experiment_root',
                        type=str,
                        help='root where to store models, losses and accuracies',
                        default='../output')

    parser.add_argument('-nep', '--epochs',
                        type=int,
                        help='number of epochs to train for',
                        default=100)

    parser.add_argument('-lr', '--learning_rate',
                        type=float,
                        help='learning rate for the model, default=0.001',
                        default=0.001)

    parser.add_argument('-lrS', '--lr_scheduler_step',
                        type=int,
                        help='StepLR learning rate scheduler step, default=20',
                        default=20)

    parser.add_argument('-lrG', '--lr_scheduler_gamma',
                        type=float,
                        help='StepLR learning rate scheduler gamma, default=0.5',
                        default=0.5)

    parser.add_argument('-its', '--iterations',
                        type=int,
                        help='number of episodes per epoch, default=100',
                        default=100)

    parser.add_argument('-cTr', '--classes_per_it_tr',
                        type=int,
                        help='number of random classes per episode for training, default=60',
                        default=60)

    parser.add_argument('-nsTr', '--num_support_tr',
                        type=int,
                        help='number of samples per class to use as support for training, default=5',
                        default=5)

    parser.add_argument('-nqTr', '--num_query_tr',
                        type=int,
                        help='number of samples per class to use as query for training, default=15',
                        default=15)

    parser.add_argument('-cVa', '--classes_per_it_val',
                        type=int,
                        help='number of random classes per episode for validation, default=5',
                        default=5)

    parser.add_argument('-nsVa', '--num_support_val',
                        type=int,
                        help='number of samples per class to use as support for validation, default=5',
                        default=5)

    parser.add_argument('-nqVa', '--num_query_val',
                        type=int,
                        help='number of samples per class to use as query for validation, default=15',
                        default=15)

    parser.add_argument('-seed', '--manual_seed',
                        type=int,
                        help='input for the manual seeds initializations',
                        default=7)

    parser.add_argument('--cuda',
                        action='store_true',
                        help='enables cuda')

    parser.add_argument('--spsa',
                        action='store_true',
                        help='use spsa prototypes')

    parser.add_argument('--all_spsa',
                        action='store_true',
                        help='use spsa prototypes for train and test')

    parser.add_argument('--reg_spsa',
                        action='store_true',
                        help='use spsa prototypes as regularization')

    parser.add_argument('--sparse',
                        action='store_true',
                        help='use sparse spsa prototypes')

    parser.add_argument('--spsa_test',
                        action='store_true',
                        help='use spsa prototypes for train and test')

    parser.add_argument('-batch', type=int, default=64, help='batch size for train without fsl')

    parser.add_argument('--sps',
                        action='store_true',
                        help='use sps for prototypes')

    parser.add_argument('--soft_nn',
                        action='store_true',
                        help='use random query')

    parser.add_argument('-temp', type=float, default=1, help='temperature for soft nn loss')

    parser.add_argument('-alpha', type=int, default=-25, help='soft nn loss multiplier')

    parser.add_argument('--random_support',
                        action='store_true',
                        help='use random query')

    parser.add_argument('-weight_decay', type=float, default=1e-3, help='weight decay for optimization')

    parser.add_argument('--eval', action='store_true', help='test model')

    parser.add_argument('-model', help='path to testing model')

    parser.add_argument('-device', type=int, default=0, help='cude device number')

    parser.add_argument('--multiloss', action='store_true', help='use multi loss')

    parser.add_argument('--tracking', action='store_true', help='use tracking learning')

    parser.add_argument('-epochs_all',
                        type=int,
                        help='number of epochs to train all net for tracking',
                        default=1)

    parser.add_argument('--multiloss_step', action='store_true', help='use step multi loss')

    parser.add_argument('-task_number', type=int, default=1, help='task number in multiloss')

    parser.add_argument('--multiloss_pretrain', action='store_true', help='use multi loss with pretraining')

    parser.add_argument('--multiloss_grad_all', action='store_true', help='use multi loss with all gradients')

    parser.add_argument('--multiloss_grad_one', action='store_true', help='use multi loss with one loss by gradients')

    parser.add_argument('--multiloss_sampling', action='store_true', help='use multi loss with task sampling')

    parser.add_argument('-top_task_number', type=int, default=1, help='task number in multiloss for top sampling')

    parser.add_argument('--loss_scale', action='store_true', help='use multi loss with loss scling params')

    parser.add_argument('--multiloss_pretrain_versa', action='store_true', help='use multi loss with pretraining vice versa')

    parser.add_argument('--multiloss_pretrain_grad_one', action='store_true',
                        help='use multi loss with pretraining adn one loss by gradients')

    parser.add_argument('--multiloss_track', action='store_true', help='use multi loss with tracking')

    parser.add_argument('--multiloss_track_pretrain', action='store_true', help='use multi loss with tracking and pretraining')

    parser.add_argument('--multiloss_sampling_pretrain', action='store_true', help='use multi loss with sampling and pretraining')

    return parser
